package conf

import "fmt"

var localConfMap = map[string]interface{}{}

const (
	AppNameKey = "app.name"
	AppEnvKey  = "app.env"
)

type ConfLoaderLocal struct {
	local  map[string]interface{}
	config *Configuration
}

func (f *ConfLoaderLocal) LoadConf(c *Configuration) error {
	fmt.Println("-----------------------开始初始化Local加载器-----------------------")
	f.config = c
	for k, v := range localConfMap {
		c.SetConfig(k, v)
	}
	return nil
}

// !!!! must call before conf.Initialize()

func SetLocalConf(k string, v interface{}) {
	localConfMap[k] = v
}
func SetAppName(name string) {
	localConfMap[AppNameKey] = name
}
func SetAppEnv(env string) {
	localConfMap[AppEnvKey] = env
}
